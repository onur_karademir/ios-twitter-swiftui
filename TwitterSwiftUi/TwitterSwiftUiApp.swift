//
//  TwitterSwiftUiApp.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI

@main
struct TwitterSwiftUiApp: App {
    var body: some Scene {
        WindowGroup {
            MainTabView()
        }
    }
}
