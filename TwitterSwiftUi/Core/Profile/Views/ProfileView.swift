//
//  ProfileView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI

struct ProfileView: View {
    @State var selectedFilter : TweetFilterViewModel = .twitts
    @Environment (\.presentationMode) var presentationMode
    var data : userDataModel
    var body: some View {
        VStack (alignment: .leading) {
            headerView
            
            actionButtons
            
            userInfoWiew
            
            tweetFilterBar
            
            tweetsView

            Spacer()
        }
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(data: data.first!)
    }
}

extension ProfileView {
    
    var headerView : some View {
        
        ZStack (alignment: .bottomLeading){
            Color(.systemBlue).ignoresSafeArea()
            
            VStack {
                Button {
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    Image(systemName: "arrow.left")
                        .resizable()
                        .frame(width: 25, height: 20)
                        .foregroundColor(.white)
                        .offset(x: 15, y: 15)
                }

                Circle()
                    .frame(width: 75, height: 75)
                    .offset(x: 15, y: 25)
            }
        }
        .frame(height: 120)

    }
    
    var actionButtons: some View {
        
        HStack (spacing: 20) {
            Spacer()
            Image(systemName: "bell.badge")
                .foregroundColor(Color.black)
                .font(.title3)
                .padding(6)
                .overlay(Circle().stroke(Color.gray, lineWidth: 1))
            Button {
                //
            } label: {
                Text("Edit Profile")
                    .fontWeight(.bold)
                    .font(.subheadline)
                    .frame(width: 120, height: 35)
                    .foregroundColor(Color.black)
                    .overlay(RoundedRectangle(cornerRadius: 20).stroke(Color.gray))
            }

        }
        .padding(.trailing)
    }
    
    var userInfoWiew : some View {

        VStack(alignment: .leading, spacing: 4) {
            HStack {
                Text(data.userName)
                    .fontWeight(.bold)
                    .font(.title2)
                Image(systemName: "checkmark.seal.fill")
                    .foregroundColor(Color.blue)
            }
            Text(data.userNick)
                .font(.subheadline)
                .foregroundColor(.gray)
            Text("Ben geceleri esen terörüm.")
                .font(.title3)
                .fontWeight(.semibold)
                .padding(.vertical)
            
            HStack (spacing: 30){
                HStack{
                    Image(systemName: "mappin.and.ellipse")
                    Text("Gotham, NY")
                }
                HStack{
                    Image(systemName: "link")
                    Text("www.batman.com")
                }
            }
            
            .foregroundColor(.gray)
            .font(.caption)
            
            HStack (spacing: 25){
                HStack (spacing: 5) {
                    Text("1")
                        .fontWeight(.bold)
                    Text("Fallowing")
                        .font(.caption)
                        .foregroundColor(Color.gray)
                }
                HStack{
                    Text("6.9M")
                        .fontWeight(.bold)
                    Text("Fallowers")
                        .font(.caption)
                        .foregroundColor(Color.gray)
                }
            }
            .padding(.vertical)
        }
        .padding(.horizontal)
    }
    
    var tweetFilterBar : some View {
        
        HStack {
            ForEach(TweetFilterViewModel.allCases, id:\.rawValue) { item in
                VStack {
                    Text(item.title)
                        .font(.subheadline)
                        .fontWeight(selectedFilter == item ? .semibold : .regular)
                        .foregroundColor(selectedFilter == item ? .black : .gray)
                    
                    if selectedFilter == item {
                        Capsule()
                            .foregroundColor(.blue)
                            .frame(height:3)
                    }else {
                        Capsule()
                            .foregroundColor(.clear)
                            .frame(height:3)
                    }
                }
                .onTapGesture {
                    withAnimation (.easeInOut){
                        self.selectedFilter = item
                    }
                }
            }
        }
        .overlay(Divider().offset(x:0, y:15))
    }
    
    var tweetsView : some View {
        
        ScrollView {
            LazyVStack {
                ForEach (0...10, id:\.self) { item in
                    TweetRowView()
                }
            }
        }
    }
}
