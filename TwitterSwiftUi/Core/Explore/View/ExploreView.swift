//
//  ExploreView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI

struct userDataModel : Identifiable {
    let id = UUID().uuidString
    let userNick : String
    let userName : String
}

let data = [
    userDataModel(userNick: "@batman", userName: "Bruce Wayne"),
    userDataModel(userNick: "@joker", userName: "Heath Ledger"),
    userDataModel(userNick: "@harvey_dent", userName: "Aaron Eckhart"),
    userDataModel(userNick: "@batman", userName: "Bruce Wayne"),
    userDataModel(userNick: "@joker", userName: "Heath Ledger"),
    userDataModel(userNick: "@harvey_dent", userName: "Aaron Eckhart"),
    userDataModel(userNick: "@batman", userName: "Bruce Wayne"),
    userDataModel(userNick: "@joker", userName: "Heath Ledger"),
    userDataModel(userNick: "@harvey_dent", userName: "Aaron Eckhart"),
    userDataModel(userNick: "@batman", userName: "Bruce Wayne")
]

struct ExploreView: View {
    var body: some View {
        NavigationView {
            
            VStack{
                ScrollView {
                    ForEach(data, id: \.id) { item in
                        NavigationLink {
                            ProfileView(data: item)
                        } label: {
                            UserRowView(userNick: item.userNick, userName: item.userName)
                        }

                    }
                }
            }
            .navigationBarTitle("Search")
            .navigationBarTitleDisplayMode(.inline)
        }

    }
}

struct ExploreView_Previews: PreviewProvider {
    static var previews: some View {
        ExploreView()
    }
}
