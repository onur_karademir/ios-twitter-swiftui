//
//  MainTabView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI

struct MainTabView: View {
    @State private var selectedIndex = 0
    var body: some View {
        TabView(selection: $selectedIndex) {
            //tab 0
            FeedView()
                .onTapGesture {
                    self.selectedIndex = 0
                }
                .tabItem {
                Image(systemName: "house")
            }.tag(0)
            
            //tab 1
            
           ExploreView()
                .onTapGesture {
                    self.selectedIndex = 1
                }
                .tabItem {
                Image(systemName: "magnifyingglass")
            }.tag(1)
            
            //tab 2
            
            NotifView()
                .onTapGesture {
                    self.selectedIndex = 2
                }
                .tabItem {
                Image(systemName: "bell")
            }.tag(2)
            
            //tab 3
            
            MessagesView()
                .onTapGesture {
                    self.selectedIndex = 3
                }
                .tabItem {
                Image(systemName: "envelope")
            }.tag(3)
        }
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
