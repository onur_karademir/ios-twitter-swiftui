//
//  TweetRowView.swift
//  TwitterSwiftUi
//
//  Created by Onur on 10.08.2022.
//

import SwiftUI

struct TweetRowView: View {
    var body: some View {
        VStack (alignment: .leading){
            //profile image and info
            HStack (alignment: .top, spacing: 12){
                //user image container
                Circle()
                    .frame(width: 56, height: 56)
                    .foregroundColor(Color(.systemBlue))
                
                VStack (alignment: .leading, spacing: 4){
                    //user info and user name
                    HStack {
                        Text("Bruce Wayne")
                            .font(.headline).bold()
                        Text("@batman")
                            .foregroundColor(.gray)
                            .font(.caption)
                        Text("2w")
                            .foregroundColor(.gray)
                            .font(.caption)
                    }
                    //tweet
                    Text("Ben geceleri esen teroörüm.")
                        .font(.subheadline)
                        .multilineTextAlignment(.leading)
                }
                
            }
            //action buttons
            
            HStack {
                Button {
                    //actions
                } label: {
                    Image(systemName: "bubble.left")
                        .font(.subheadline)
                }
                Spacer()
                Button {
                    //actions
                } label: {
                    Image(systemName: "arrow.2.squarepath")
                        .font(.subheadline)
                }
                Spacer()
                Button {
                    //actions
                } label: {
                    Image(systemName: "heart")
                        .font(.subheadline)
                }
                Spacer()
                Button {
                    //actions
                } label: {
                    Image(systemName: "bookmark")
                        .font(.subheadline)
                }

            }
            .padding()
            .foregroundColor(.gray)
            Divider()
        }
        .padding()
    }
}

struct TweetRowView_Previews: PreviewProvider {
    static var previews: some View {
        TweetRowView()
    }
}
